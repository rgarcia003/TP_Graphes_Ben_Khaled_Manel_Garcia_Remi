#include <iostream>
#include <deque>
#include <algorithm>
#include <SFML/Graphics.hpp>
#include "Graphe.h"
#include "Sommet.h"
#include "Arete.h"
#include "Appli.h"
#include<map>
using namespace std;

///////////////////////////////
// Exercice 1.1
///////////////////////////////
/// \brief calculer_c3
/// \param g
/// \param e
///
///
void calculer_c3(Graphe & g, Arete e){
    //Affecte a "e" le résultat de la division
    //du nombre de cycles de taille 3 passant
    //par "e" par le nombre de cycles de taille
    //3 dans tout le graphe.
    //1 "tous les voisins de mes extrémités sont voisins entre eux".

    Sommet source = g.source(e);
    Sommet destination=g.destination(e);

    vector <Sommet> sommets_communs;
    vector <Sommet> sommets_communs_max;

    set<Sommet> voisins_source= g.voisins(source);
    set<Sommet> voisins_dest= g.voisins(destination);

    set_intersection(voisins_source.begin(), voisins_source.end(),voisins_dest.begin(), voisins_dest.end(),back_inserter(sommets_communs));
    set_union(voisins_source.begin(), voisins_source.end(),voisins_dest.begin(), voisins_dest.end(),back_inserter(sommets_communs_max));

    float c3=sommets_communs.size()*1.0/sommets_communs_max.size()*1.0;
    //cout << c3 << endl;

    //Affecter à e la valeur c3
    g.metriqueArete(e, c3);

}

///////////////////////////////
// Exercice 1.2
///////////////////////////////
void calculer_mesure(Graphe &g){
    for (auto i:g.aretes())
        calculer_c3(g,i);
}
///////////////////////////////
// Exercice 2.1
///////////////////////////////
void filtrer_arete(Graphe &g, float borne){

    for (auto it: g.aretes())
    {
        if(g.metriqueArete(it) < borne)
            g.supprimerArete(it);
    }
}

///////////////////////////////
// Exercice 2.2
///////////////////////////////

set<Sommet>DFS(Graphe &g,Sommet s, set<Sommet>& parcourus,set<Sommet>& foret_intermediaire,set<Sommet> &sommets)
{
    parcourus.insert(s);
    foret_intermediaire.insert(s);
    sommets.erase(s);
    //si le sommet voisin n'est pas parcouru, on lance la recursion sur lui

    for(auto premier_voisin: g.voisins(s)){

        if(parcourus.find(premier_voisin)==parcourus.end())
        {
            DFS(g,premier_voisin,parcourus,foret_intermediaire,sommets);
        }
    }

    return foret_intermediaire;
}

void calculer_composantes_connexes(Graphe &g, vector<set<Sommet>> & comp_connexes)
{

    set<Sommet> sommets_parcourus;
    set<Sommet> foret_intermediaire;
    auto sommets= g.sommets();

    while (sommets_parcourus.size()!=g.sommets().size())
    {
        //On redéfinit le premier sommet non marque:

        auto premier_non_marque=sommets.begin();

        foret_intermediaire.clear();

        //On stocke toute la forêt retournée par le DFS
        comp_connexes.push_back(DFS(g,*premier_non_marque,sommets_parcourus,foret_intermediaire,sommets));

        //le dfs sera relancé si tout le graphe n'est pas parcouru

    }

}

///////////////////////////////
// Exercice 2.3
///////////////////////////////
void trouver_groupes(Graphe g, float borne, vector<set<Sommet>> & groupes){

    calculer_mesure(g);

    filtrer_arete(g, borne);

    vector<set<Sommet>> comp_connexes;

    calculer_composantes_connexes(g,comp_connexes);
    for(auto it: comp_connexes)
        groupes.push_back(it);



}

///////////////////////////////
// Exercice 3.1.1
///////////////////////////////
float densite_intra(Graphe &g, set<Sommet> & c)
{
    //c.densite= nombre d'arêtes dans la communauté
    //sur nombre d'arêtes d'un graphe complet de |Ci| sommets.

    float densite;
    //On utilise un set pour annuler les doublons.
    set<Arete> aretes;

    for (auto s: c)
    {

        for(auto i:g.incidentes(s))
        {
            if(find(c.begin(),c.end(),g.destination(i))!=c.end() && find(c.begin(),c.end(),g.source(i))!=c.end() )
                aretes.insert(i);

        }

        //on remplit le set d'arete avec.
    }
    //nb d'arêtes dans la communauté:
    int nb_aretes_communaute=aretes.size();

    //nb d'arêtes dans un graphe complet de |Ci| sommets:
    int nb_aretes_graphe_complet= (c.size()*(c.size()-1))/2;

    if (nb_aretes_graphe_complet==0)
        densite=0;
    else
        densite=(1.0*nb_aretes_communaute)/(1.0*nb_aretes_graphe_complet);


    return densite;
}

///////////////////////////////
// Exercice 3.1.2
///////////////////////////////
float densite_intra(Graphe &g, vector<set<Sommet> >& communautes){
    float densite;

    float somme_densites=0;
    int nb_communautes=communautes.size();

    for (auto c: communautes)
        somme_densites+=densite_intra(g,c);

    densite=(1.0*somme_densites)/(1.0*nb_communautes);

    return densite;
}

///////////////////////////////
// Exercice 3.2.1

///////////////////////////////
//Calcule la densité d'arêtes entre les communautés.
float densite_inter(Graphe &g, set<Sommet> &c_i, set<Sommet> c_j){

    float densite;
    set<Arete> aretes;
    //  int aretes_reliant=0;
    int aretes_max=0;


    //le ratio entre

    //nombre d'arêtes reliant les sommets de c i et de c j
    for (auto s: c_i)
    {
        for (auto i: g.incidentes(s))//si l'arete pointe vers un sommet de c_j et part de c_i ou l'inverse
            if((find(c_j.begin(),c_j.end(),g.destination(i))!=c_j.end() && find(c_i.begin(),c_i.end(),g.source(i))!=c_i.end())
                    || (find(c_i.begin(),c_i.end(),g.destination(i))!=c_i.end() && find(c_j.begin(),c_j.end(),g.source(i))!=c_j.end())
                    )

                aretes.insert(i);

        //on remplit le set d'arete avec.
    }

    //et le nombre maximum possible de telles arêtes, i.e. |c i ||c j |
    aretes_max= c_i.size()*c_j.size();

    if(aretes_max==0)
        densite=0;
    else
        densite=(aretes.size()*1.0)/(aretes_max*1.0);

    return densite;

}

///////////////////////////////
// Exercice 3.2.2
///////////////////////////////
float densite_inter(Graphe &g, vector<set<Sommet> >& communautes){
    float densite;

    float somme_densites=0;
    int nb_communautes=communautes.size();
    // j est constamment un rang au dela de i, on teste tous les couples(i,j) possibles
    for(auto c_i=communautes.begin();c_i!=prev(communautes.end());++c_i)
    {
        for(auto c_j= c_i+1;c_j!=communautes.end();++c_j)
        {
            somme_densites+=densite_inter(g,*c_i,*c_j);

        }
    }
    densite=(1.0*somme_densites)/(1.0*nb_communautes*(nb_communautes-1)/2);//peut faire 0/0 avec un graphe connexe


    return densite;
}

///////////////////////////////
// Exercice 3.3.1
///////////////////////////////
float evaluer_qualite(Graphe g, vector<set<Sommet> > & comp_connexes){
    //s'il n'y a qu'une communauté, le calcul de la densité inter n'a aucun sens
    if(comp_connexes.size()>1)
        return densite_intra(g,comp_connexes)-densite_inter(g,comp_connexes);

    else
        return -1;//puisqu'il n'y a meme pas de decomposition, on considère que c'est le pire des cas


}

///////////////////////////////
// Exercice 3.3.2
///////////////////////////////
float trouver_meilleure_borne(Graphe &g, float pas = 0.1f){

    cout<<"Veuillez patienter le temps du calcul de la meilleure borne... "<<endl;
    map <float, float> meilleure_borne;
    vector<set<Sommet>> composantes_connexes;
    for(float borne=0;borne<=1;borne+=pas)
    {
        composantes_connexes.clear();
        trouver_groupes(g, borne,composantes_connexes);
        meilleure_borne[evaluer_qualite(g,composantes_connexes)]= borne;
    }

    auto max=  max_element(meilleure_borne.begin(), meilleure_borne.end(), meilleure_borne.value_comp());
    cout<<"La plus grande qualité est de: "<<max->first<<endl;
    return max->second;
}

///////////////////////////////
// Exercice 4
///////////////////////////////
void colorier_sommets(Graphe & g, vector<set<Sommet> > & comp_connexes)
{
    int red=0;
    int blue=0;
    int green=0;
    Couleur c={red, green ,blue,255};
    for (auto composante: comp_connexes)
    {
        red=(red+47)%255;
        green =(green+69)%255;
        blue=(blue+39)%255;
        for (auto sommet: composante)
        {
            c={red,green,blue,255};
            g.couleurSommet(sommet,c);
        }
    }
}


int main()
{
    // chargement et création des graphe et fenetre d'affichage
    string fichier = "graphe.txt";
    Graphe g;
    if(!g.charger(fichier)){
        cout << "Erreur de chargement de fichier" << endl;
        return 1;
    }





    Appli app;
    app.setGraphe(g);


    ////////////////////////////////
    // Exercice 1
    ///////////////////////////////
    // //Question 1.1:
    //calculer_c3(g,*g.aretes().begin());

    // //Question 1.2:
    //calculer_mesure(g);


    ////////////////////////////////
    // Exercice 2
    ///////////////////////////////
    // //Question 2.1/2.2
    //    calculer_mesure(g);

    //    filtrer_arete(g, 0.1);

    //    vector<set<Sommet>> comp_connexes;
    //    calculer_composantes_connexes(g,comp_connexes);

    // //   Question 2.3
    //    vector<set<Sommet>> groupes;
    //    trouver_groupes(g,0.8,groupes);


    ////////////////////////////////
    // Exercice 3
    ///////////////////////////////
    // //Question  3.1.1/3.2.2:
    // densite_intra(g,groupes);

    // //Question 3.2.1/3.2.2
    //      densite_inter(g,groupes);

    // //Question 3.3
    //        float meilleure_borne=trouver_meilleure_borne(g);
    //        cout << meilleure_borne << endl;



    ////////////////////////////////
    // Exercice 4
    ///////////////////////////////
    //    vector<set<Sommet>> groupes;
    //    trouver_groupes(g,0.5,groupes);
    //    colorier_sommets(g,groupes);



    ////////////////////////////////
    // Exercice 5
    ///////////////////////////////
    vector<set<Sommet>> groupes;
    //Calcul de la meilleure borne:
    float meilleure_borne=trouver_meilleure_borne(g);
    cout <<"La borne donnant le meilleur decoupage est: "<< meilleure_borne << endl;

    //Calcul du nombre de communauté
    trouver_groupes(g, meilleure_borne, groupes);
    cout << "Le nombre de communautés correspondant est: "<<groupes.size() << endl;

    //Coloriage du graphe:
    colorier_sommets(g, groupes);

    app.run();
    return 0;
}

