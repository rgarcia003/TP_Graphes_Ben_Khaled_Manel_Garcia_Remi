TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += Appli.cpp Arete.cpp Coord.cpp Couleur.cpp Graphe.cpp main.cpp Sommet.cpp

HEADERS += Appli.h Arete.h Coord.h Couleur.h Graphe.h Sommet.h \
    Propriete.hpp

QMAKE_CXXFLAGS += -std=c++11 -Wall -Wextra
LIBS           += -lsfml-graphics -lsfml-window -lsfml-system 

# décommenter pour les tests BOOST
#LIBS          +=  -lboost_unit_test_framework
